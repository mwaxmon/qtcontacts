#include <QQmlApplicationEngine>
#include <QtQuickTest/quicktest.h>

int main(int argc, char* argv[]){
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine; //Just needed to force windeployqt to output needed libs and plugins.

    return quick_test_main(argc, argv, "contact_view_test", ":/");
}

