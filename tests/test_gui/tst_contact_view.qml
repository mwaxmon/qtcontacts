import QtQuick 2.0
import QtTest 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls 2.3
import "qrc:/app"

Rectangle {
    width: 640
    height: 480

    ContactView {
        id: contactView
        visible: true
        anchors.fill: parent
        contact: { "name": "test", "uuid": "test_guid" }
    }

    TestCase {
        name: "contact_view_test"
        when: windowShown

        SignalSpy {
            id: deleteSpy
            target: contactView
            signalName: "deleteContact"
        }

        SignalSpy {
            id: editSpy
            target: contactView
            signalName: "edit"
        }

        SignalSpy {
            id: mapSpy
            target: contactView
            signalName: "showMap"
        }

        function test_delete_signal() {
            compare(deleteSpy.valid, true)
            compare(deleteSpy.count, 0)
            compare(contactView.visible, true)
            var button = findChild(contactView, "deleteButton")
            compare(button != null, true, "Could not find button.  Please check if name has changed.")
            mouseClick(button)
            compare(deleteSpy.count, 1)
            compare(deleteSpy.signalArguments[0][0].toString(), "test_guid")
        }

        function test_edit_signal() {
            compare(editSpy.valid, true)
            compare(editSpy.count, 0)
            compare(contactView.visible, true)
            var editButton = findChild(contactView, "editButton")
            compare(editButton != null, true, "Could not find button.  Please check if name has changed.")
            mouseClick(editButton)
            compare(editSpy.count, 1)
        }

        function test_show_map() {
            var repeater = findChild(contactView, "addressRepeater")

            //TODO: Determine why model cannot be set using list of objects
            //repeater.model = [{"street": "test_street", "city": "test_city", "state": "test_state", "zip": "test_zip"}]
            repeater.model = Qt.createQmlObject('import QtQuick 2.0; ListModel { ListElement { street: "test_street"; city: "test_city"; state: "test_state"; zip: "test_zip" }}', contactView, '')

            compare(mapSpy.valid, true)
            compare(mapSpy.count, 0)
            compare(contactView.visible, true)

            var showMapButton = findChild(contactView, "goToMapButton", "Could not find button.  Please check if name has changed")
            compare(showMapButton != null, true)

            mouseClick(showMapButton)

            compare(mapSpy.count, 1)
            compare(mapSpy.signalArguments.length, 1)

            var addressData = mapSpy.signalArguments[0][0]

            compare(addressData.street, "test_street")
            compare(addressData.city, "test_city")
            compare(addressData.state, "test_state")
            compare(addressData.zip, "test_zip")
        }
    }
}