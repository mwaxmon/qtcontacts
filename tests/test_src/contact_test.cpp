#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <QSignalSpy>
#include "contact.h"

class ContactTest : public ::testing::Test {
    protected:
};

TEST_F(ContactTest, TestNameChangedSignal) {
    Contact c1;
    QSignalSpy spy(&c1, SIGNAL(nameChanged()));

    ASSERT_THAT(c1.Name(), QString());
    ASSERT_NE(c1.Uuid(), QString());

    QString newName("test");
    c1.SetName(newName);

    ASSERT_THAT(spy.count(), 1);
    ASSERT_THAT(c1.Name(), newName);
}

TEST_F(ContactTest, TestContactNumberChanged) {
    ContactNumber cn;
    ASSERT_THAT(cn.Number(), QString());
    ASSERT_THAT(cn.NumberType(), "Work");

    QString newNumber("1-658-561-6853");
    QString newType("Mobile");
    cn.setNumber(newNumber);
    cn.setType(newType);

    ASSERT_THAT(cn.Number(), newNumber);
    ASSERT_THAT(cn.NumberType(), newType);
}

TEST_F(ContactTest, TestContactEmailChanged) {
    ContactEmail ce;
    ASSERT_THAT(ce.Email(), QString());
    ASSERT_THAT(ce.EmailType(), "Work");

    QString newEmail("test@test.test");
    QString newType("Home");

    ce.setEmail(newEmail);
    ce.setType(newType);

    ASSERT_THAT(ce.Email(), newEmail);
    ASSERT_THAT(ce.EmailType(), newType);
}

TEST_F(ContactTest, TestContactAddressChanged) {
    ContactAddress ca;
    ASSERT_THAT(ca.Type(), "Home");
    ASSERT_THAT(ca.Street(), QString());
    ASSERT_THAT(ca.City(), QString());
    ASSERT_THAT(ca.State(), QString());
    ASSERT_THAT(ca.ZipCode(), QString());

    QString newStreet("123 Something Ave.");
    QString newCity("New City");
    QString newState("New State");
    QString newZip("00000");
    QString newType("Home");

    ca.setStreet(newStreet);
    ca.setCity(newCity);
    ca.setState(newState);
    ca.setZipCode(newZip);
    ca.setType(newType);

    ASSERT_THAT(ca.Type(), newType);
    ASSERT_THAT(ca.Street(), newStreet);
    ASSERT_THAT(ca.City(), newCity);
    ASSERT_THAT(ca.State(), newState);
    ASSERT_THAT(ca.ZipCode(), newZip);
    ASSERT_THAT(ca.Type(), newType);
}