import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3


Item {
    property var contact : contactModel
    signal edit()
    signal deleteContact(string uuid)
    signal showMap(var addressInfo)

    RowLayout {
        anchors.right: parent.right
        id: buttons
        layoutDirection: Qt.RightToLeft
        RoundButton {
            flat: true
            id: deleteButton
            objectName: "deleteButton"
            icon.source: "icons/delete_black_48px.svg"
            onClicked: deleteContact(contact.uuid)
        }
        RoundButton {
            flat: true
            id: editButton
            objectName: "editButton"
            icon.source: "icons/create_black_48px.svg"
            onClicked: edit()
        }
    }

    ScrollView {
        anchors.top: buttons.bottom
        anchors.bottom: parent.bottom
        Layout.fillWidth: true
        width: parent.width
        id: scrollView
        clip: true
        ColumnLayout {
            Layout.fillWidth: true
            width: scrollView.width
            spacing: 0
            Label {
                id: nameLabel
                Layout.fillWidth: true
                text: contact.name
                leftPadding: 10
                width: scrollView.width
            }
            Label {
                id: phoneLabel
                Layout.fillWidth: true
                text: "Phone"
                leftPadding: 20
                width: scrollView.width
                background: Rectangle {
                    color: "grey"
                    border.color: "white"
                    width: scrollView.width
                }
            }
            Repeater {
                Layout.fillWidth: true
                model : contactNumbersModel
                width: scrollView.width
                onCountChanged: phoneLabel.visible = (count != 0)
                ItemDelegate {
                    Layout.fillWidth: true
                    width: scrollView.width
                    text: model.number + "<br>" + model.type
                }
            }
            Label {
                id: emailLabel
                Layout.fillWidth: true
                text: "Email"
                leftPadding: 20
                width: scrollView.width
                background: Rectangle {
                    color: "grey"
                    border.color: "white"
                    width: scrollView.width
                }
            }
            Repeater {
                Layout.fillWidth: true
                model : contactEmailsModel
                width: scrollView.width
                onCountChanged: emailLabel.visible = (count != 0)
                ItemDelegate {
                    Layout.fillWidth: true
                    text: model.email + "<br>" + model.type
                    width: scrollView.width
                }
            }
            Label {
                id: addressLabel
                Layout.fillWidth: true
                text: "Address"
                leftPadding: 20
                width: scrollView.width
                background: Rectangle {
                    color: "grey"
                    border.color: "white"
                    width: scrollView.width
                }
            }
            Repeater {
                id: addressRepeater
                objectName: "addressRepeater"
                Layout.fillWidth: true
                model: contactAddressesModel
                width: scrollView.width
                onCountChanged: addressLabel.visible = (count != 0)
                RowLayout {
                    ItemDelegate {
                        id: addressText
                        Layout.fillWidth: true
                        width: scrollView.width
                        text: model.street + "<br>" + model.city + ", " + model.state + "  " + model.zip + "<br>" + model.type
                    }
                    RoundButton {
                        flat: true
                        id: goToMapButton
                        objectName: "goToMapButton"
                        icon.source: "icons/place_black_48px.svg"
                        Layout.alignment: Qt.AlignRight
                        onClicked: showMap({street: model.street, city: model.city, state: model.state, zip: model.zip})
                    }
                }
            }
        }
    }
}
