import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    signal save()
    signal cancel()
    signal addNumber()
    signal removeNumber(int index)
    signal addEmail()
    signal removeEmail(int index)
    signal addAddress()
    signal removeAddress(int index)

    property var types : ["Work", "Home", "Mobile"]
    property var contact: contactModel

    ScrollView {
        anchors.top: parent.top
        anchors.bottom: contactOperationButtons.top
        Layout.fillWidth: true
        width: parent.width
        id: scrollView
        clip: true
        ColumnLayout {
            Layout.fillWidth: true
            width: scrollView.width
            spacing: 0
            TextField {
                Layout.fillWidth: true
                text: contact.name
                selectByMouse: true
                onEditingFinished: contact.name = text
                leftPadding: 10
                width: scrollView.width
            }
            Repeater {
                id: phoneRepeater
                Layout.fillWidth: true
                model : contactNumbersModel
                width: scrollView.width
                RowLayout {
                    layoutDirection: Qt.RightToLeft
                    property string type : model.type
                    onTypeChanged: model.type = type
                    RoundButton {
                        icon.source: "icons/clear_black_48px.svg"
                        flat: true
                        onClicked: removeNumber(model.index)
                    }
                    ComboBox {
                        id: numberTypeCombo
                        model : types
                        Component.onCompleted: currentIndex = find(parent.type)
                        onActivated: type = displayText
                    }
                    TextField {
                        Layout.fillWidth: true
                        text: model.number
                        selectByMouse: true
                        onEditingFinished: model.number = text
                        placeholderText: "Phone"
                    }
                }
            }
            RowLayout {
                RoundButton {
                    id: addPhoneButton
                    Layout.alignment: Qt.AlignHCenter
                    icon.source: "icons/add_black_48px.svg"
                    onClicked: addNumber()
                }
                Label {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignLeft
                    text: "Add phone"
                }
            }

            Repeater {
                id: emailRepeater
                Layout.fillWidth: true
                model : contactEmailsModel
                width: scrollView.width
                RowLayout {
                    layoutDirection: Qt.RightToLeft
                    property var type: model.type
                    onTypeChanged: model.type = type
                    RoundButton {
                        icon.source: "icons/clear_black_48px.svg"
                        flat: true
                        onClicked: removeEmail(model.index)
                    }
                    ComboBox {
                        model: types
                        Component.onCompleted: currentIndex = find(parent.type)
                        onActivated: parent.type = displayText
                    }
                    TextField {
                        Layout.fillWidth: true
                        text: model.email
                        selectByMouse: true
                        onEditingFinished: model.email = text
                        placeholderText: "Email"
                    }
                }
            }
            RowLayout {
                RoundButton {
                    id: addEmailButton
                    Layout.alignment: Qt.AlignHCenter
                    icon.source: "icons/add_black_48px.svg"
                    onClicked: addEmail()
                }
                Label {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignLeft
                    text: "Add email"
                }
            }
            Repeater {
                id: addressRepeater
                Layout.fillWidth: true
                model: contactAddressesModel
                width: scrollView.width
                RowLayout {
                    Layout.fillWidth: true
                    ColumnLayout {
                        Layout.fillWidth: true
                        TextField {
                            Layout.fillWidth: true
                            text: model.street
                            selectByMouse: true
                            onEditingFinished: model.street = text
                            placeholderText: "Street"
                        }
                        TextField {
                            Layout.fillWidth: true
                            text: model.city
                            selectByMouse: true
                            onEditingFinished: model.city = text
                            placeholderText: "City"
                        }
                        TextField {
                            Layout.fillWidth: true
                            text: model.state
                            selectByMouse: true
                            onEditingFinished: model.state = text
                            placeholderText: "State"
                        }
                        TextField {
                            Layout.fillWidth: true
                            text: model.zip
                            selectByMouse: true
                            onEditingFinished: model.zip = text
                            placeholderText: "ZIP code"
                        }
                    }
                    ColumnLayout {
                        id: addressTypeComboColumn
                        Layout.alignment: Qt.AlignTop
                        property var type: model.type
                        onTypeChanged: model.type = type
                        ComboBox {
                            model: types
                            Component.onCompleted: currentIndex = find(parent.type)
                            onActivated: parent.type = displayText
                        }
                    }
                    ColumnLayout {
                        id: deleteAddressButtonColumn
                        Layout.alignment: Qt.AlignTop
                        RoundButton {
                            icon.source: "icons/clear_black_48px.svg"
                            flat: true
                            onClicked: removeAddress(model.index)
                        }
                    }
                }
            }
            RowLayout {
                RoundButton {
                    Layout.alignment: Qt.AlignHCenter
                    icon.source: "icons/add_black_48px.svg"
                    onClicked: addAddress()
                }
                Label {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignLeft
                    text: "Add address"
                }
            }
        }
    }
    RowLayout {
        id: contactOperationButtons
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        Button {
            flat: true
            text: "CANCEL"
            onClicked: cancel()
        }
        Button {
            flat: true
            text: "SAVE"
            onClicked: save()
        }
    }
}