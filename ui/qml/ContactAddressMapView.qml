import QtQuick 2.0
import QtLocation 5.10
import QtPositioning 5.6

Item {
    function populateDestination(addressInfo) {
        destinationAddress.street = addressInfo.street
        destinationAddress.city = addressInfo.city
        destinationAddress.state = addressInfo.state
        destinationAddress.postalCode = addressInfo.zip

        geocodeModel.query = destinationAddress
        geocodeModel.update()
    }

    Plugin {
        id: mapPlugin
        name: "esri"
    }

    Address {
        id: destinationAddress
    }

    GeocodeModel {
        id: geocodeModel
        plugin: map.plugin
        onStatusChanged: {
            if ((status == GeocodeModel.Ready) || (status == GeocodeModel.Error))
            map.geocodeFinished()
        }
        onLocationsChanged:
        {
            if (count == 1) {
                map.center = get(0).coordinate
                contactAddressMarker.coordinate = get(0).coordinate
            }
        }
    }

    MapQuickItem {
        id: contactAddressMarker
        sourceItem: Image {
            source: "icons/person_pin_circle_48px.svg"
        }
    }

    Map {
        id: map
        anchors.fill: parent
        plugin: mapPlugin
        zoomLevel: 14
        gesture.enabled: true
        gesture.acceptedGestures: MapGestureArea.PinchGesture | MapGestureArea.PanGesture | MapGestureArea.RotationGesture

    }

    Component.onCompleted: {
        for(var i = 0; i < map.supportedMapTypes.length; i++) {
            //console.debug(map.supportedMapTypes[i].name, map.supportedMapTypes[i].style)
            if(map.supportedMapTypes[i].style === MapType.SatelliteMapDay)
                map.activeMapType = map.supportedMapTypes[i]
        }
        contactAddressMarker.z = map.z+1
        map.addMapItem(contactAddressMarker)
    }
}