import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: contactListView
    objectName: "contactListView"
    signal selectedContactChanged(string contactUuid)
    signal addContactClicked()
    signal deleteContact(string contactUuid)
    signal editContact(string contactUuid)

    RowLayout {
        id: searchRow
        objectName: "searchTextField"
        anchors.left: parent.left
        anchors.right: parent.right
        TextField {
            id: searchTextField
            placeholderText: "Search Contacts"
            Layout.fillWidth: true
            selectByMouse: true
            onTextChanged: {
                contactsModel.setFilterString(text)
                if(text.length > 0)
                    clearSearchButton.enabled = true
                else
                    clearSearchButton.enabled = false
            }
        }
        Button {
            id: clearSearchButton
            Layout.alignment: Qt.AlignRight
            icon.source: "icons/clear_black_48px.svg"
            enabled: false
            flat: true
            onClicked: searchTextField.clear()
        }
    }
    ScrollView {
        anchors.top: searchRow.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        id: scrollView
        clip: true
        ListView {
            id: listView
            anchors.fill: parent
            model: contactsModel
            delegate: ItemDelegate {
                text: model.name
                background.implicitWidth: parent.width
                onClicked: selectedContactChanged(model.uuid)
                onPressAndHold: contextMenu.popup()
                Menu {
                    id: contextMenu
                    MenuItem {
                        text: model.name
                        font.bold: true
                    }
                    MenuItem {
                        text: "Delete Contact"
                        onTriggered: deleteContact(model.uuid)
                    }
                    MenuItem {
                        text: "Edit Contact"
                        onTriggered: editContact(model.uuid)
                    }
                }
            }
            section.property: "name"
            section.criteria: ViewSection.FirstCharacter
            section.delegate: Label {
                text: section
            }
        }
    }
    RoundButton {
        id: addButton
        objectName: "addButton"
        z: 1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        icon.source: "icons/add_black_48px.svg"
        onClicked: addContactClicked()
    }
}
