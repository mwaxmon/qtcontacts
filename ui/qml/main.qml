import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: applicationWindow
    objectName: "applicationWindow"
    visible: true
    width: 300
    height: 600
    title: qsTr("QtContacts")

    ContactListView {
        visible: false
        id: contactListView
        objectName: "contactListView"
        onSelectedContactChanged: stackView.push(contactView)
        onAddContactClicked: stackView.push(contactEditView)
        onEditContact: stackView.push(contactEditView)
    }

    ContactView {
        visible: false
        id: contactView
        objectName: "contactView"
        onEdit: {
            stackView.push(contactEditView)
        }
        onShowMap: {
            mapView.populateDestination(addressInfo)
            stackView.push(mapView)
        }
        onDeleteContact: stackView.pop()
    }

    ContactEditView {
        visible: false
        id: contactEditView
        objectName: "contactEditView"
        onCancel: stackView.pop()
        onSave: {
            stackView.pop()
            if(stackView.currentItem === contactListView)
            {
                stackView.push(contactView)
            }
        }
    }

    ContactAddressMapView {
        id: mapView
    }

    StackView {
        id: stackView
        anchors.bottom: buttonLayout.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        initialItem: contactListView
    }

    RowLayout {
        id: buttonLayout
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        RoundButton {
            id: backButton
            icon.source: "icons/back_black_48px.svg"
            onClicked: stackView.pop()
        }

        RoundButton {
            id: homeButton
            icon.source: "icons/home_black_48px.svg"
            onClicked: stackView.pop(null)
        }
    }
}
