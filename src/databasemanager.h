#ifndef CONTACTSAPP_DATABASEMANAGER_H
#define CONTACTSAPP_DATABASEMANAGER_H

#include <QObject>
#include <QVector>
#include <QtSql/QtSql>

class Contact;
class ContactEmail;
class ContactAddress;
class ContactNumber;

class DatabaseManager: public QObject {
    Q_OBJECT
public:
    DatabaseManager();
    void saveContact(QSharedPointer<Contact> contactToSave);
    void resetContact(QSharedPointer<Contact> contactToReset);
    bool contactExists(QStringView uuid);
    void deleteContact(QStringView uuid);
    QVector<QSharedPointer<Contact>> queryDatabase();
private:
    std::unique_ptr<QSqlTableModel> mSqlTableModel;
    static QSharedPointer<Contact> createContact(QStringView uuid, QJsonDocument document);

    static QVector<QSharedPointer<ContactAddress>> parseAddresses(const QJsonArray& jsonAddresses);
    static QVector<QSharedPointer<ContactEmail>> parseEmails(const QJsonArray& jsonEmails);
    static QVector<QSharedPointer<ContactNumber>> parseNumbers(const QJsonArray& jsonNumbers);

    static const QString contactJsonNameKey;

    static const QString contactJsonNumbersKey;
    static const QString contactJsonEmailsKey;
    static const QString contactJsonAddressesKey;

    static const QString contactJsonStreetKey;
    static const QString contactJsonCityKey;
    static const QString contactJsonStateKey;
    static const QString contactJsonZipKey;
    static const QString contactJsonTypeKey;

    static const QString sqlTableName;
    static const QString sqlDatabaseName;

    static const QString sqlTableKey;
    static const QString sqlTableValue;
};

#endif //CONTACTSAPP_DATABASEMANAGER_H
