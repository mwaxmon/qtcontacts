#include "databasemanager.h"
#include "contact.h"
#include <functional>
#include <iostream>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

//TODO: Investigate clang-tidy warning cert-err58-cpp
const QString DatabaseManager::contactJsonNameKey = "name";

const QString DatabaseManager::contactJsonNumbersKey = "numbers";
const QString DatabaseManager::contactJsonEmailsKey = "emails";
const QString DatabaseManager::contactJsonAddressesKey = "addresses";

const QString DatabaseManager::contactJsonStreetKey = "street";
const QString DatabaseManager::contactJsonCityKey = "city";
const QString DatabaseManager::contactJsonStateKey = "state";
const QString DatabaseManager::contactJsonZipKey = "zip";
const QString DatabaseManager::contactJsonTypeKey = "type";

const QString DatabaseManager::sqlTableName = "contacts";
const QString DatabaseManager::sqlDatabaseName = "test.db";

const QString DatabaseManager::sqlTableKey = "ID";
const QString DatabaseManager::sqlTableValue = "JSON";


DatabaseManager::DatabaseManager() {
    auto db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(sqlDatabaseName);
    db.open();

    QString createTableStatement = QString("CREATE TABLE IF NOT EXISTS %1 (%2 TEXT PRIMARY KEY NOT NULL, %3 TEXT, UNIQUE(%4))")
                                   .arg(sqlTableName)
                                   .arg(sqlTableKey)
                                   .arg(sqlTableValue)
                                   .arg(sqlTableKey);
    auto result = db.exec(createTableStatement);
    if(result.lastError().isValid()) {
        qDebug() << result.lastError().text();
    }

    mSqlTableModel = std::make_unique<QSqlTableModel>(this, db);
    mSqlTableModel->setTable(sqlTableName);
    mSqlTableModel->select();
}

void DatabaseManager::saveContact(QSharedPointer<Contact> contactToSave) {
    QJsonObject contactJson;
    contactJson[contactJsonNameKey] = contactToSave->Name();

    QJsonArray numbers;
    std::for_each(contactToSave->NumbersList()->begin(), contactToSave->NumbersList()->end(), [&numbers](const auto& number) {
        QJsonObject jsonNumber;
        jsonNumber[number->Number()] = number->NumberType();
        numbers << jsonNumber;
    });
    if(numbers.count() > 0)
        contactJson[contactJsonNumbersKey] = numbers;

    QJsonArray emails;
    std::for_each(contactToSave->EmailsList()->begin(), contactToSave->EmailsList()->end(), [&emails](const auto& email) {
        QJsonObject jsonEmail;
        jsonEmail[email->Email()] = email->EmailType();
        emails << jsonEmail;
    });
    if(emails.count() > 0)
        contactJson[contactJsonEmailsKey] = emails;

    QJsonArray addresses;
    std::for_each(contactToSave->AddressList()->begin(), contactToSave->AddressList()->end(), [&addresses](const auto& address){
        QJsonObject jsonAddress;
        jsonAddress[contactJsonStreetKey] = address->Street();
        jsonAddress[contactJsonCityKey]   = address->City();
        jsonAddress[contactJsonStateKey]  = address->State();
        jsonAddress[contactJsonZipKey]    = address->ZipCode();
        jsonAddress[contactJsonTypeKey]   = address->Type();
        addresses << jsonAddress;
    });
    if(addresses.count() > 0)
        contactJson[contactJsonAddressesKey] = addresses;

    QJsonDocument jsonDocument(contactJson);
    QString strJson(jsonDocument.toJson(QJsonDocument::Compact));

    auto saveStatement = QString("REPLACE INTO %1 (%2, %3) VALUES ('%4', '%5')")
                        .arg(sqlTableName)
                        .arg(sqlTableKey)
                        .arg(sqlTableValue)
                        .arg(contactToSave->Uuid())
                        .arg(strJson);
    auto result = mSqlTableModel->database().exec(saveStatement);

    if(result.lastError().isValid()) {
        qDebug() << result.lastError().text();
    }
}

void DatabaseManager::resetContact(QSharedPointer<Contact> contactToReset) {
    auto queryString = QString("SELECT %1 FROM %2 WHERE %3 = '%4'")
                       .arg(sqlTableValue)
                       .arg(sqlTableName)
                       .arg(sqlTableKey)
                       .arg(contactToReset->Uuid());
    auto query = mSqlTableModel->database().exec(queryString);
    if(query.next())
    {
        QJsonDocument jsonDoc = QJsonDocument::fromJson(query.value(0).toString().toUtf8());

        QJsonObject jsonContact = jsonDoc.object();

        contactToReset->SetName(jsonContact[contactJsonNameKey].toString());

        contactToReset->NumbersList()->clear();
        auto currentNumbers = parseNumbers(jsonContact[contactJsonNumbersKey].toArray());
        contactToReset->NumbersList()->swap(currentNumbers);

        contactToReset->EmailsList()->clear();
        auto currentEmails = parseEmails(jsonContact[contactJsonEmailsKey].toArray());
        contactToReset->EmailsList()->swap(currentEmails);

        contactToReset->AddressList()->clear();
        auto currentAddresses = parseAddresses(jsonContact[contactJsonAddressesKey].toArray());
        contactToReset->AddressList()->swap(currentAddresses);
    }
}

void DatabaseManager::deleteContact(QStringView uuid) {
    auto deleteStatement = QString("DELETE FROM %1 WHERE %2 = '%3'").arg(sqlTableName).arg(sqlTableKey).arg(uuid);
    auto result = mSqlTableModel->database().exec(deleteStatement);
    if(result.lastError().isValid()) {
        qDebug() << result.lastError().text();
    }
}

QVector<QSharedPointer<Contact>> DatabaseManager::queryDatabase() {
    QVector<QSharedPointer<Contact>> contacts;
    for(int i = 0; i < mSqlTableModel->rowCount(); i++) {
        QString id = mSqlTableModel->record(i).value(sqlTableKey).toString();
        QJsonDocument json = QJsonDocument::fromJson(mSqlTableModel->record(i).value(sqlTableValue).toString().toUtf8());
        contacts.append(createContact(id, json));
    }
    return contacts;
}

QSharedPointer<Contact> DatabaseManager::createContact(QStringView uuid, QJsonDocument document) {
    QJsonObject jsonContact = document.object();

    QString name = jsonContact[contactJsonNameKey].toString();
    QVector<QSharedPointer<ContactNumber>> numbers = parseNumbers(jsonContact[contactJsonNumbersKey].toArray());
    QVector<QSharedPointer<ContactEmail>> emails = parseEmails(jsonContact[contactJsonEmailsKey].toArray());
    QVector<QSharedPointer<ContactAddress>> addresses = parseAddresses(jsonContact[contactJsonAddressesKey].toArray());

    return QSharedPointer<Contact>::create(uuid.toString(),
                                           name,
                                           QSharedPointer<QVector<QSharedPointer<ContactNumber>>>::create(numbers),
                                           QSharedPointer<QVector<QSharedPointer<ContactEmail>>>::create(emails),
                                           QSharedPointer<QVector<QSharedPointer<ContactAddress>>>::create(addresses));
}

bool DatabaseManager::contactExists(QStringView uuid) {
    auto queryString = QString("SELECT %1 FROM %2 WHERE %3 = '%4'").arg(sqlTableValue).arg(sqlTableName).arg(sqlTableKey).arg(uuid);
    auto query = mSqlTableModel->database().exec(queryString);
    return query.next();
}

QVector<QSharedPointer<ContactAddress>> DatabaseManager::parseAddresses(const QJsonArray &jsonAddresses) {
    QVector<QSharedPointer<ContactAddress>> parsedAddresses;
    for(const auto& jsonAddress : jsonAddresses) {
        QJsonObject jsonAddressObject = jsonAddress.toObject();
        auto parsedAddress = QSharedPointer<ContactAddress>::create(jsonAddressObject.value(contactJsonStreetKey).toString(),
                                                                    jsonAddressObject.value(contactJsonCityKey).toString(),
                                                                    jsonAddressObject.value(contactJsonStateKey).toString(),
                                                                    jsonAddressObject.value(contactJsonZipKey).toString(),
                                                                    jsonAddressObject.value(contactJsonTypeKey).toString());
        parsedAddresses.append(parsedAddress);
    }
    return parsedAddresses;
}

QVector<QSharedPointer<ContactEmail>> DatabaseManager::parseEmails(const QJsonArray &jsonEmails) {
    QVector<QSharedPointer<ContactEmail>> parsedEmails;
    for(const auto& jsonEmail : jsonEmails) {
        auto jsonEmailObject = jsonEmail.toObject();
        QString emailString = jsonEmailObject.keys().first();
        auto parsedEmail = QSharedPointer<ContactEmail>::create(emailString,
                                                                jsonEmailObject.value(emailString).toString());
        parsedEmails.append(parsedEmail);
    }
    return parsedEmails;
}

QVector<QSharedPointer<ContactNumber>> DatabaseManager::parseNumbers(const QJsonArray &jsonNumbers) {
    QVector<QSharedPointer<ContactNumber>> parsedNumbers;
    for(const auto& jsonNumber : jsonNumbers) {
        auto jsonNumberObject = jsonNumber.toObject();
        QString numberString = jsonNumberObject.keys().first();
        auto parsedNumber = QSharedPointer<ContactNumber>::create(numberString,
                                                                  jsonNumberObject.value(numberString).toString());
        parsedNumbers.append(parsedNumber);
    }
    return parsedNumbers;
}
