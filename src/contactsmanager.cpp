#include "contactsmanager.h"
#include "contact.h"

const std::function<bool(QSharedPointer<Contact>, QString)> ContactsManager::compareInSearchFunction =
        [](const auto& contact, const auto& uuid)
            {return contact->Uuid().compare(uuid) < 0;};

ContactsManager::ContactsManager(QObject *parent) : QObject(parent), mContacts()
{
    mDatabaseManager = std::make_unique<DatabaseManager>();
    mContacts.append(mDatabaseManager->queryDatabase());

    // TODO: Benchmark sort of list from DB followed by merge
    std::sort(mContacts.begin(),
              mContacts.end(),
              [](const auto& contactA, const auto& contactB)
                { return contactA->Uuid().compare(contactB->Uuid()) < 0; });

    for(auto& contact : mContacts) {
        QObject::connect(contact.data(), &Contact::nameChanged, [this, &contact](){ emit nameChanged(indexOf(contact->Uuid())); });
    }
}

QVector<QSharedPointer<Contact>> ContactsManager::contacts() const {
    return mContacts;
}

void ContactsManager::appendItem(QSharedPointer<Contact> c)
{
    auto insertBeforeIndex = std::lower_bound(mContacts.begin(),
                                              mContacts.end(),
                                              c->Uuid(),
                                              compareInSearchFunction);
    mContacts.insert(insertBeforeIndex, c);
    QObject::connect(c.data(), &Contact::nameChanged, [c, this]{ emit nameChanged(indexOf(c->Uuid())); });
    emit userAppended(mContacts.size() - 1);
}

void ContactsManager::removeItem(QStringView uuid)
{
    int index = indexOf(uuid);
    if(index >= 0) {
        userAboutToBeRemoved(index);
        auto toRemove = mContacts[index];
        toRemove->disconnect(this);
        mContacts.remove(index);
        mDatabaseManager->deleteContact(uuid);
    }
}

int ContactsManager::indexOf(QStringView uuid) const {
    const auto uuidString = uuid.toString();
    return std::distance(mContacts.begin(),
                          std::lower_bound(mContacts.begin(),
                                           mContacts.end(),
                                           uuidString,
                                           compareInSearchFunction));
}

QSharedPointer<Contact> ContactsManager::findContact(QStringView uuid) const {
    int index = indexOf(uuid);
    if(index >= 0 && index < mContacts.count())
        return mContacts[index];
    return nullptr;
}

void ContactsManager::saveItem(QSharedPointer<Contact> contactToSave) {
    mDatabaseManager->saveContact(contactToSave);
}

void ContactsManager::resetContact(QSharedPointer<Contact> contactToReset) {
    mDatabaseManager->resetContact(contactToReset);
}

bool ContactsManager::contactSavedPreviously(QSharedPointer<Contact> contactToCheck) {
    return mDatabaseManager->contactExists(contactToCheck->Uuid());
}
