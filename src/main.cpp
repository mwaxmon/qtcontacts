#include <QObject>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "contactsmanager.h"
#include "contactslistmodel.h"
#include "signalconnector.h"
#include "contactssearchfiltermodel.h"
#include "contactemaillistmodel.h"
#include "contactnumberlistmodel.h"
#include "contactaddresslistmodel.h"
#include "contactmodel.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    auto contactsManager = QSharedPointer<ContactsManager>::create();
    auto contactModel = QSharedPointer<ContactModel>::create();
    ContactsListModel contactsModel;
    ContactsSearchFilterModel searchFilterModel;
    auto emailsModel = QSharedPointer<ContactEmailListModel>::create();
    auto numbersModel = QSharedPointer<ContactNumberListModel>::create();
    auto addressesModel = QSharedPointer<ContactAddressListModel>::create();


    ObjectConnector connector(contactsManager, emailsModel, numbersModel, addressesModel, contactModel);

    contactsModel.setContacts(contactsManager);

    searchFilterModel.setSourceModel(&contactsModel);
    searchFilterModel.setFilterRole(ContactsListModel::NameRole);
    searchFilterModel.setSortRole(ContactsListModel::NameRole);
    searchFilterModel.setSortCaseSensitivity(Qt::CaseInsensitive);
    searchFilterModel.setFilterCaseSensitivity(Qt::CaseInsensitive);
    searchFilterModel.sort(0);

    engine.rootContext()->setContextProperty("contactsModel", &searchFilterModel);
    engine.rootContext()->setContextProperty("contactEmailsModel", emailsModel.data());
    engine.rootContext()->setContextProperty("contactNumbersModel", numbersModel.data());
    engine.rootContext()->setContextProperty("contactAddressesModel", addressesModel.data());
    engine.rootContext()->setContextProperty("contactModel", contactModel.data());

    engine.load(QUrl(QStringLiteral("qrc:/app/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    auto editView = engine.rootObjects().first()->findChild<QObject*>("contactEditView");
    connector.connectSave(editView);
    connector.connectCancel(editView);
    connector.connectAddNumber(editView);
    connector.connectRemoveNumber(editView);
    connector.connectAddEmail(editView);
    connector.connectRemoveEmail(editView);
    connector.connectAddAddress(editView);
    connector.connectRemoveAddress(editView);

    auto contactListView = engine.rootObjects().first()->findChild<QObject*>("contactListView");
    connector.connectDelete(contactListView);
    connector.connectContactSelected(contactListView);
    connector.connectAddNewContact(contactListView);
    connector.connectEditContact(contactListView);

    auto contactView = engine.rootObjects().first()->findChild<QObject*>("contactView");
    connector.connectDelete(contactView);

    return app.exec();
}
