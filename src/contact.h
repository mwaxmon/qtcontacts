#ifndef CASSANDRAVIEWER_CONTACT_H
#define CASSANDRAVIEWER_CONTACT_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QSharedPointer>
#include <QUuid>

class ContactAddress : public QObject {
    Q_OBJECT
public:
    explicit ContactAddress(QString street, QString city, QString state, QString zipCode, QString type, QObject* parent = nullptr)
        : QObject(parent), street(street), city(city), state(state), zipCode(zipCode), type(type) {}
    explicit ContactAddress(QObject* parent = nullptr) : QObject(parent), street(""), city(""), state(""), zipCode(""), type("Home") {}

    QString Street()  { return street;  }
    QString City()    { return city;    }
    QString State()   { return state;   }
    QString ZipCode() { return zipCode; }
    QString Type()    { return type;    }

    void setStreet(QString newStreet)   { street = newStreet;   }
    void setCity(QString newCity)       { city = newCity;       }
    void setState(QString newState)     { state = newState;     }
    void setZipCode(QString newZipCode) { zipCode = newZipCode; }
    void setType(QString newType)       { type = newType;       }

private:
    QString street;
    QString city;
    QString state;
    QString zipCode;
    QString type;
};

class ContactNumber : public QObject
{
    Q_OBJECT

public:
    explicit ContactNumber(QString number, QString type, QObject* parent = nullptr) : number(number), type(type), QObject(parent) {}
    explicit ContactNumber(QObject *parent = nullptr) : QObject(parent), number(""), type("Work") {}

    QString Number() const { return number; }
    QString NumberType() const { return type; }

    void setNumber(QString number) { this->number = number; }
    void setType(QString type) { this->type = type; }

private:
    QString number;
    QString type;
};

class ContactEmail : public QObject
{
    Q_OBJECT

public:
    explicit ContactEmail(QString email, QString type, QObject *parent = nullptr) : email(email), type(type), QObject(parent) {}
    explicit ContactEmail(QObject *parent = nullptr) : QObject(parent), email(""), type("Work") {}

    QString Email() const { return email; }
    QString EmailType() const { return type; }

    void setEmail(QString email) { this->email = email; }
    void setType(QString type) { this->type = type; }

private:
    QString email;
    QString type;
};

class Contact : public QObject
{
    Q_OBJECT
public:
    explicit Contact(QObject *parent = nullptr) :
            QObject(parent),
            name(""),
            uuid(""),
            numbers(QSharedPointer<QVector<QSharedPointer<ContactNumber>>>::create()),
            emails(QSharedPointer<QVector<QSharedPointer<ContactEmail>>>::create()),
            addresses(QSharedPointer<QVector<QSharedPointer<ContactAddress>>>::create())
    {
        uuid = QUuid::createUuid().toString();
    }

    Contact(QString uuid,
            QString name,
            QSharedPointer<QVector<QSharedPointer<ContactNumber>>> numbers,
            QSharedPointer<QVector<QSharedPointer<ContactEmail>>> emails,
            QSharedPointer<QVector<QSharedPointer<ContactAddress>>> addresses,
            QObject *parent = nullptr) :
                QObject(parent),
                uuid(uuid),
                name(name),
                numbers(numbers),
                emails(emails),
                addresses(addresses)
    {}

    QString Uuid() const { return uuid; }
    QString Name() const { return name; }

    void SetName(QString name) { this->name = name; emit nameChanged(); }
    QSharedPointer<QVector<QSharedPointer<ContactEmail>>> EmailsList() { return emails; }
    QSharedPointer<QVector<QSharedPointer<ContactNumber>>> NumbersList() { return numbers; }
    QSharedPointer<QVector<QSharedPointer<ContactAddress>>> AddressList() { return addresses; }

signals:
    void nameChanged();

private:
    QString uuid;
    QString name;
    QSharedPointer<QVector<QSharedPointer<ContactNumber>>> numbers;
    QSharedPointer<QVector<QSharedPointer<ContactEmail>>> emails;
    QSharedPointer<QVector<QSharedPointer<ContactAddress>>> addresses;
};


//Q_DECLARE_METATYPE(types)

#endif //CASSANDRAVIEWER_CONTACT_H
