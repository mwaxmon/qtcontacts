#include "contactnumberlistmodel.h"
#include "contact.h"

ContactNumberListModel::ContactNumberListModel(QObject *parent) : QAbstractListModel(parent) {

}

int ContactNumberListModel::rowCount(const QModelIndex &parent) const {
    if(parent.isValid())
        return 0;

    return mNumbers.isNull() ? 0 : mNumbers->count();
}

QVariant ContactNumberListModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid())
        return QVariant();

    const auto& contactNumber = mNumbers->at(index.row());

    switch(role) {
        case NumberRole:
            return QVariant(contactNumber->Number());
        case TypeRole:
            return QVariant(contactNumber->NumberType());
    }
    return QVariant();
}

bool ContactNumberListModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    auto contactNumber = mNumbers->at(index.row());
    auto newValue = value.toString();
    if(role == NumberRole && contactNumber->Number() != newValue) {
        contactNumber->setNumber(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    else if(role == TypeRole && contactNumber->NumberType() != newValue) {
        contactNumber->setType(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ContactNumberListModel::flags(const QModelIndex &index) const {
    if(!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

bool ContactNumberListModel::insertRows(int row, int count, const QModelIndex &parent) {
    beginInsertRows(parent, row, row + count - 1);
    mNumbers->push_back(QSharedPointer<ContactNumber>::create());
    endInsertRows();
	return true;
}

bool ContactNumberListModel::removeRows(int row, int count, const QModelIndex &parent) {
    beginRemoveRows(parent, row, row + count - 1);
    mNumbers->remove(row);
    endRemoveRows();
	return true;
}

QHash<int, QByteArray> ContactNumberListModel::roleNames() const {
    QHash<int, QByteArray> names;
    names[NumberRole] = "number";
    names[TypeRole] = "type";
    return names;
}

void ContactNumberListModel::setNumbers(QSharedPointer<QVector<QSharedPointer<ContactNumber>>> numbers) {
    beginResetModel();
    mNumbers = numbers;
    endResetModel();
}

void ContactNumberListModel::invalidateData() {
    mNumbers = nullptr;
}

void ContactNumberListModel::resetModel() {
    beginResetModel();
    endResetModel();
}
