#ifndef CASSANDRAVIEWER_CONTACTNUMBERLISTMODEL_H
#define CASSANDRAVIEWER_CONTACTNUMBERLISTMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>

class ContactNumber;

class ContactNumberListModel : public QAbstractListModel {
    Q_OBJECT

public:
    explicit ContactNumberListModel(QObject* parent = nullptr);
    enum roles {
        NumberRole = Qt::UserRole,
        TypeRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    virtual QHash<int, QByteArray> roleNames() const override;

    void setNumbers(QSharedPointer<QVector<QSharedPointer<ContactNumber>>> numbers);
    void invalidateData();
    void resetModel();

private:
    QSharedPointer<QVector<QSharedPointer<ContactNumber>>> mNumbers;
};


#endif //CASSANDRAVIEWER_CONTACTNUMBERLISTMODEL_H
