#ifndef CASSANDRAVIEWER_CONTACTSSEARCHFILTERMODEL_H
#define CASSANDRAVIEWER_CONTACTSSEARCHFILTERMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>

class ContactsSearchFilterModel : public QSortFilterProxyModel {
    Q_OBJECT
public:
    ContactsSearchFilterModel(QObject* parent = 0);
    Q_INVOKABLE void setFilterString(QString string);
protected:
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
};


#endif //CASSANDRAVIEWER_CONTACTSSEARCHFILTERMODEL_H
