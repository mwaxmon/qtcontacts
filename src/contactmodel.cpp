#include "contactmodel.h"
#include <QDebug>
#include "contact.h"

ContactModel::ContactModel(QObject *parent) : QObject(parent) {

}

QString ContactModel::name() const {
    return mContact.isNull() ? QString() : mContact->Name();
}

void ContactModel::setName(QString newName) {
    if(!mContact.isNull())
        mContact->SetName(newName);
    nameChanged(newName);
}

void ContactModel::setContact(QSharedPointer<Contact> newContact) {
    mContact = newContact;
    nameChanged(mContact->Name());
}

QSharedPointer<Contact> ContactModel::currentContact() const {
    return mContact;
}

QString ContactModel::uuid() const {
    return mContact->Uuid();
}

void ContactModel::invalidateData() {
    mContact = nullptr;
}
