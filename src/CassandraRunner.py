from cassandra.cqlengine import columns
from cassandra.cqlengine import connection
from cassandra.cqlengine.models import Model
from cassandra.cqlengine.management import sync_table
from cassandra.cqlengine.query import LWTException


class Users(Model):
    email = columns.Text(primary_key=True)
    first_name = columns.Text()
    last_name = columns.Text()
    age = columns.Integer()
    city = columns.Text()


class UserDbConnection:
    def __init__(self):
        connection.setup(['127.0.0.1'], 'demo')
        sync_table(Users)

    def update_user(self, key, property_name, new_value):
        if property_name == 'email':
            def update_primary_key(old_key, new_key):
                if old_key == new_key:
                    return False

                old_user = Users.objects().get(email=old_key)
                new_user = Users(email=new_key,
                                 first_name=old_user.first_name,
                                 last_name=old_user.last_name,
                                 city=old_user.city,
                                 age=old_user.age)

                try:
                    new_user.if_not_exists().save()
                    old_user.delete()
                except LWTException as e:
                    return False
                return True
            return update_primary_key(key, new_value)
        else:
            user = Users.objects(email=key).only({property_name}).limit(1)

            if user.count() == 1 and getattr(user[0], property_name) != new_value:
                user.update(**{property_name: new_value})
                return True
            return False

    def create_new_user(self, key):
        try:
            Users.objects(email=key).if_not_exists().create(email=key)
        except LWTException as e:
            return False
        return True

    def delete_user(self, key):
        Users.objects(email=key).limit(1).delete()

    def get_users(self):
        for user in Users.objects().fetch_size(10):
            yield user
