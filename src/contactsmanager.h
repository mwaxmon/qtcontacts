#ifndef USERS_H
#define USERS_H

#include <QObject>
#include <QVector>
#include <functional>
#include "databasemanager.h"

class Contact;

class ContactsManager : public QObject
{
    Q_OBJECT

public:
    explicit ContactsManager(QObject *parent = nullptr);

    QVector<QSharedPointer<Contact>> contacts() const;
    QSharedPointer<Contact> findContact(QStringView uuid) const;

    void appendItem(QSharedPointer<Contact> contact);
    void removeItem(QStringView uuid);

    void saveItem(QSharedPointer<Contact> contactToSave);
    void resetContact(QSharedPointer<Contact> contactToReset);

    bool contactSavedPreviously(QSharedPointer<Contact> contactToCheck);

signals:
    void userAppended(int index);

    void userAboutToBeRemoved(int index);

    void nameChanged(int index);

private:
    QVector<QSharedPointer<Contact>> mContacts;

    int indexOf(QStringView uuid) const;
    std::unique_ptr<DatabaseManager> mDatabaseManager;

    static const std::function<bool(QSharedPointer<Contact>, QString)> compareInSearchFunction;
};

#endif // USERS_H
