#ifndef CASSANDRAVIEWER_CONTACTMODEL_H
#define CASSANDRAVIEWER_CONTACTMODEL_H

#include <QObject>
#include <QSharedPointer>

class Contact;

class ContactModel : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString uuid READ uuid)
public:
    explicit ContactModel(QObject* parent = nullptr);
    QString name() const;
    QString uuid() const;
    void setName(QString newName);
    void setContact(QSharedPointer<Contact> newContact);
    QSharedPointer<Contact> currentContact() const;

    void invalidateData();
signals:
    void nameChanged(QString name);

private:
    QSharedPointer<Contact> mContact;
};


#endif //CASSANDRAVIEWER_CONTACTMODEL_H
