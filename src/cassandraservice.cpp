#include <cmath>
#include <Python.h>
#include "cassandraservice.h"

//TODO: Put error checking after python calls.
CassandraService::CassandraService() : dbConnection(nullptr)
{
    auto pName = PyUnicode_DecodeFSDefault("CassandraRunner");
    auto pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    auto pObjects = PyModule_GetDict(pModule);
    Py_DECREF(pModule);
    auto pythonClass = PyDict_GetItemString(pObjects, "UserDbConnection");
    Py_DECREF(pObjects);
    dbConnection = PyObject_CallObject(pythonClass, nullptr);
}

std::vector<User> CassandraService::getUsers()
{
    std::vector<User> users;
    auto userIterator = PyObject_CallMethod(dbConnection, "get_users", "()");
    auto user = PyIter_Next(userIterator);
    while(user != NULL)
    {
        auto values = PyMapping_Items(user);
        User u;
        for(int i = 0; i < PySequence_Length(values); i++) {
            auto tuple = PySequence_GetItem(values, i);

            auto property = PyTuple_GetItem(tuple, 0);
            auto repr = PyObject_Repr(property);
            auto str = PyUnicode_AsEncodedString(repr, "utf-8", "");
            auto bytes = PyBytes_AsString(str);
            std::string propertyName = std::string(bytes);
            propertyName = propertyName.substr(1, propertyName.length() - 2); // Removing extra ' character encoding.
            Py_DECREF(repr);
            Py_DECREF(str);

            auto val = PyTuple_GetItem(tuple, 1);
            auto repr2 = PyObject_Repr(val);
            auto str2 = PyUnicode_AsEncodedString(repr2, "utf-8", "");
            auto bytes2 = PyBytes_AsString(str2);
            std::string value = bytes2;
            if(value[0] == '\'' && value[value.length() - 1] == '\'')
                value = value.substr(1, value.length() - 2);
            Py_DECREF(repr2);
            Py_DECREF(str2);

            Py_DECREF(tuple);

            if(propertyName == "email")
                u.email = QString::fromStdString(value);
            else if(propertyName == "first_name")
                u.firstName = QString::fromStdString(value);
            else if(propertyName == "last_name")
                u.lastName = QString::fromStdString(value);
            else if(propertyName == "city")
                u.city = QString::fromStdString(value);
            else if(propertyName == "age")
                u.age = (value == "None" || value.empty()) ? 0 : std::stoi(value);
        }
        Py_DECREF(user);
        Py_DECREF(values);
        users.push_back(u);
        user = PyIter_Next(userIterator);
    }

    Py_DECREF(userIterator);

    return users;
}

CassandraService::~CassandraService()
{
//    Py_DECREF(dbConnection);
}

bool CassandraService::createObject(std::string key)
{
    auto result = PyObject_CallMethod(dbConnection, "create_new_user", "(s)", key.c_str());
    bool toReturn = PyObject_IsTrue(result);

    if(result)
        Py_DECREF(result);
    else
        PyErr_Print();

    return toReturn;
}

bool CassandraService::updateObject(std::string key, std::string column, std::string newValue)
{
    auto result = PyObject_CallMethod(dbConnection, "update_user", "(sss)", key.c_str(), column.c_str(), newValue.c_str());
    bool toReturn = PyObject_IsTrue(result);
    if(result)
        Py_DECREF(result);
    else
        PyErr_Print();

    return toReturn;
}

void CassandraService::deleteObject(std::string key) {
    auto result = PyObject_CallMethod(dbConnection, "delete_user", "(s)", key.c_str());
    if(result)
        Py_DECREF(result);
    else
        PyErr_Print();
}
