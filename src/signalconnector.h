#ifndef CASSANDRAVIEWER_SIGNALCONNECTOR_H
#define CASSANDRAVIEWER_SIGNALCONNECTOR_H

#include <QObject>
#include <QVariant>
#include <functional>
#include <QDebug>
#include "contact.h"
#include "contactsmanager.h"
#include "contactemaillistmodel.h"
#include "contactnumberlistmodel.h"
#include "contactaddresslistmodel.h"
#include "contactmodel.h"

// TODO: This does more than just connect signals.  Needs refactoring.
class ObjectConnector : public QObject
{
    Q_OBJECT
public:
    ObjectConnector(QSharedPointer<ContactsManager> contactsManager,
                    QSharedPointer<ContactEmailListModel> emails,
                    QSharedPointer<ContactNumberListModel> numbers,
                    QSharedPointer<ContactAddressListModel> addresses,
                    QSharedPointer<ContactModel> contactModel) :
                        mContactsManager(contactsManager),
                        mEmailsListModel(emails),
                        mNumbersListModel(numbers),
                        mAddressesListModel(addresses),
                        mContactModel(contactModel)
    {
    }

    void connectSave(QObject *signaler) {
        QObject::connect(signaler, SIGNAL(save()), this, SLOT(handleSave()));
    }

    void connectCancel(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(cancel()), this, SLOT(handleCancel()));
    }

    void connectAddNewContact(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(addContactClicked()), this, SLOT(handleCreateNewContact()));
    }

    void connectDelete(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(deleteContact(QString)), this, SLOT(deleteContact(QString)));
    }

    void connectAddNumber(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(addNumber()), this, SLOT(addNumber()));
    }

    void connectRemoveNumber(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(removeNumber(int)), this, SLOT(handleRemoveNumber(int)));
    }

    void connectAddEmail(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(addEmail()), this, SLOT(handleAddEmail()));
    }

    void connectRemoveEmail(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(removeEmail(int)), this, SLOT(handleRemoveEmail(int)));
    }

    void connectAddAddress(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(addAddress()), this, SLOT(handleAddAddress()));
    }

    void connectRemoveAddress(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(removeAddress(int)), this, SLOT(handleRemoveAddress(int)));
    }

    void connectContactSelected(QObject* signaler) {
        QObject::connect(signaler, SIGNAL(selectedContactChanged(QString)), this, SLOT(handleContactSelected(QString)));
    }

    void connectEditContact(QObject *signaler) {
        QObject::connect(signaler, SIGNAL(editContact(QString)), this, SLOT(handleContactEditSelected(QString)));
    }

private slots:
    void handleContactEditSelected(const QString &uuid) {
        SetupCurrentContact(uuid);
    };

    void handleSave() {
        auto contact = mContactModel->currentContact();
        mContactsManager->saveItem(contact);
    }

    void handleCancel() {
        auto contact = mContactModel->currentContact();
        if (mContactsManager->contactSavedPreviously(contact)) {
            mContactsManager->resetContact(contact);
            mEmailsListModel->resetModel();
            mNumbersListModel->resetModel();
            mAddressesListModel->resetModel();
        }
        else {
            mContactsManager->removeItem(contact->Uuid());
            mEmailsListModel->invalidateData();
            mNumbersListModel->invalidateData();
            mAddressesListModel->invalidateData();
            mContactModel->invalidateData();
        }
    }

    void handleCreateNewContact() {
        auto newContact = QSharedPointer<Contact>::create();
        mContactsManager->appendItem(newContact);
        mContactModel->setContact(newContact);
        mEmailsListModel->setEmails(newContact->EmailsList());
        mNumbersListModel->setNumbers(newContact->NumbersList());
        mAddressesListModel->setAddresses(newContact->AddressList());
    }

    void deleteContact(const QString &uuid) {
        mContactsManager->removeItem(uuid);
    }

    void addNumber() {
        mNumbersListModel->insertRow(mNumbersListModel->rowCount(QModelIndex()));
    }

    void handleRemoveNumber(int index) {
        mNumbersListModel->removeRow(index);
    }

    void handleAddEmail() {
        mEmailsListModel->insertRow(mEmailsListModel->rowCount(QModelIndex()));
    }

    void handleRemoveEmail(int index) {
        mEmailsListModel->removeRow(index);
    }

    void handleAddAddress() {
        mAddressesListModel->insertRow(mAddressesListModel->rowCount(QModelIndex()));
    }

    void handleRemoveAddress(int index) {
        mAddressesListModel->removeRow(index);
    }

    void handleContactSelected(const QString &uuid) {
        SetupCurrentContact(uuid);
    }

private:
    QSharedPointer<ContactsManager> mContactsManager;
    QSharedPointer<ContactEmailListModel> mEmailsListModel;
    QSharedPointer<ContactNumberListModel> mNumbersListModel;
    QSharedPointer<ContactAddressListModel> mAddressesListModel;
    QSharedPointer<ContactModel> mContactModel;

    void SetupCurrentContact(QStringView uuid) {
        auto contact = mContactsManager->findContact(uuid);
        mContactModel->setContact(contact);
        mNumbersListModel->setNumbers(contact->NumbersList());
        mEmailsListModel->setEmails(contact->EmailsList());
        mAddressesListModel->setAddresses(contact->AddressList());
    }
};

#endif //CASSANDRAVIEWER_SIGNALCONNECTOR_H
