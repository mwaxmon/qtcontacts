#ifndef CASSANDRAVIEWER_CONTACTEMAILLISTMODEL_H
#define CASSANDRAVIEWER_CONTACTEMAILLISTMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>

class ContactEmail;

class ContactEmailListModel : public QAbstractListModel {
    Q_OBJECT
public:
    explicit ContactEmailListModel(QObject* parent = nullptr);

    enum roles {
        EmailRole = Qt::UserRole,
        TypeRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    virtual QHash<int, QByteArray> roleNames() const override;

    void setEmails(QSharedPointer<QVector<QSharedPointer<ContactEmail>>> emails);

    void resetModel();
    void invalidateData();

private:
    QSharedPointer<QVector<QSharedPointer<ContactEmail>>> mEmails;
};

#endif //CASSANDRAVIEWER_CONTACTEMAILLISTMODEL_H
