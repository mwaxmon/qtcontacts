#ifndef CASSANDRASERVICE_H
#define CASSANDRASERVICE_H
//
//#include <cmath>
//#include <Python.h>
#include <string>
#include <vector>
#include "user.h"

class CassandraService {
private:
    PyObject* dbConnection;
public:
    CassandraService();
    ~CassandraService();

    std::vector<User> getUsers();
    bool updateObject(std::string key, std::string column, std::string newValue);
    bool createObject(std::string key);
    void deleteObject(std::string key);
};


#endif //CASSANDRASERVICE_H
