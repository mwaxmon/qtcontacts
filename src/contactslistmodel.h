#ifndef CONTACTSMODEL_H
#define CONTACTSMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>

class ContactsManager;

class ContactsListModel : public QAbstractListModel
{
public:
    explicit ContactsListModel(QObject *parent = nullptr);

    enum roles {
        NameRole = Qt::UserRole,
        UuidRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    virtual QHash<int, QByteArray> roleNames() const override;

    void setContacts(QSharedPointer<ContactsManager> contactsManager);

private:
    QSharedPointer<ContactsManager> mContactsManager;
};

#endif // CONTACTSMODEL_H
