#include "contactssearchfiltermodel.h"

ContactsSearchFilterModel::ContactsSearchFilterModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

void ContactsSearchFilterModel::setFilterString(QString string)
{
    this->setFilterFixedString(string);
}

bool ContactsSearchFilterModel::lessThan(const QModelIndex &left, const QModelIndex &right) const {
    auto leftData = sourceModel()->data(left, sortRole());
    auto rightData = sourceModel()->data(right, sortRole());
    return leftData.toString() < rightData.toString();
}
