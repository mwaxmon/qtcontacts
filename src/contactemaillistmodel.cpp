#include "contactemaillistmodel.h"
#include "contact.h"

ContactEmailListModel::ContactEmailListModel(QObject *parent) : QAbstractListModel(parent) {

}

int ContactEmailListModel::rowCount(const QModelIndex &parent) const {
    if(parent.isValid())
        return 0;

    return mEmails.isNull() ? 0 : mEmails->count();
}

QVariant ContactEmailListModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid())
        return QVariant();

    const auto& email = mEmails->at(index.row());

    switch(role){
        case EmailRole:
            return QVariant(email->Email());
        case TypeRole:
            return QVariant(email->EmailType());
    }
    return QVariant();
}

bool ContactEmailListModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    auto email = mEmails->at(index.row());
    auto newValue = value.toString();
    if(role == EmailRole && email->Email() != newValue) {
        email->setEmail(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    else if(role == TypeRole && email->EmailType() != newValue) {
        email->setType(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ContactEmailListModel::flags(const QModelIndex &index) const {
    if(!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

bool ContactEmailListModel::insertRows(int row, int count, const QModelIndex &parent) {
    beginInsertRows(parent, row, row + count - 1);
    if(row == mEmails->count() - 1)
        for(int i = 0; i < count; i++)
            mEmails->push_back(QSharedPointer<ContactEmail>::create());
    else
        for(int i = 0; i < count; i++)
            mEmails->insert(row+i, QSharedPointer<ContactEmail>::create());
    endInsertRows();
	return true;
}

bool ContactEmailListModel::removeRows(int row, int count, const QModelIndex &parent) {
    beginRemoveRows(parent, row, row + count - 1);
    mEmails->remove(row, count);
    endRemoveRows();
	return true;
}

QHash<int, QByteArray> ContactEmailListModel::roleNames() const {
    QHash<int, QByteArray> names;
    names[EmailRole]  = "email";
    names[TypeRole] = "type";
    return names;
}

void ContactEmailListModel::setEmails(QSharedPointer<QVector<QSharedPointer<ContactEmail>>> emails) {
    beginResetModel();
    mEmails = emails;
    endResetModel();
}

void ContactEmailListModel::resetModel() {
    beginResetModel();
    endResetModel();
}

void ContactEmailListModel::invalidateData() {
    mEmails = nullptr;
}
