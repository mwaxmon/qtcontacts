#include "contactslistmodel.h"
#include "contactsmanager.h"
#include "contact.h"

ContactsListModel::ContactsListModel(QObject *parent)
    : QAbstractListModel(parent),
      mContactsManager(nullptr)
{
}

int ContactsListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !mContactsManager)
        return 0;

    return mContactsManager->contacts().size();
}

QVariant ContactsListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !mContactsManager)
        return QVariant();

    auto contact = mContactsManager->contacts().at(index.row());
    switch(role) {
        case NameRole:
            return QVariant(contact->Name());
        case UuidRole:
            return QVariant(contact->Uuid());
    }
    return QVariant();
}

bool ContactsListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    return false;
}

Qt::ItemFlags ContactsListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

bool ContactsListModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // Handled in manager
    endInsertRows();
	return true;
}

bool ContactsListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // Handled in manager.
    endRemoveRows();
	return true;
}

QHash<int, QByteArray> ContactsListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[NameRole] = "name";
    names[UuidRole] = "uuid";
    return names;
}

void ContactsListModel::setContacts(QSharedPointer<ContactsManager> contactsManager)
{
    beginResetModel();

    if(mContactsManager != nullptr)
        mContactsManager->disconnect(this);

    mContactsManager = contactsManager;

    connect(mContactsManager.data(), &ContactsManager::nameChanged, [this](int index) {
        emit dataChanged(createIndex(index, 0), createIndex(index, 0), QVector<int>() << NameRole);
    });

    connect(mContactsManager.data(), &ContactsManager::userAppended, [this](int index) {
        insertRow(index, QModelIndex());
    });
    connect(mContactsManager.data(), &ContactsManager::userAboutToBeRemoved, [this](int index) {
        removeRow(index, QModelIndex());
    });

    endResetModel();
}

