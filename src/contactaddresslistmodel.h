#ifndef CONTACTSAPP_CONTACTADDRESSMODEL_H
#define CONTACTSAPP_CONTACTADDRESSMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>

class ContactAddress;

class ContactAddressListModel : public QAbstractListModel {
    Q_OBJECT
public:
    explicit ContactAddressListModel(QObject* parent = nullptr);

    enum roles {
        StreetRole = Qt::UserRole,
        CityRole,
        StateRole,
        ZipRole,
        TypeRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    virtual QHash<int, QByteArray> roleNames() const override;

    void setAddresses(QSharedPointer<QVector<QSharedPointer<ContactAddress>>> addresses);

    void invalidateData();
    void resetModel();
private:
    QSharedPointer<QVector<QSharedPointer<ContactAddress>>> mAddresses;
};


#endif //CONTACTSAPP_CONTACTADDRESSMODEL_H
