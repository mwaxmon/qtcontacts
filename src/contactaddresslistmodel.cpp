#include "contactaddresslistmodel.h"
#include "contact.h"

ContactAddressListModel::ContactAddressListModel(QObject *parent) : QAbstractListModel(parent) {

}

int ContactAddressListModel::rowCount(const QModelIndex &parent) const {
    if(parent.isValid())
        return 0;

    return mAddresses.isNull() ? 0 : mAddresses->count();
}

QVariant ContactAddressListModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid())
        return QVariant();

    const auto& address = mAddresses->at(index.row());

    switch(role) {
        case StreetRole:
            return QVariant(address->Street());
        case CityRole:
            return QVariant(address->City());
        case StateRole:
            return QVariant(address->State());
        case ZipRole:
            return QVariant(address->ZipCode());
        case TypeRole:
            return QVariant(address->Type());
    }
    return QVariant();
}

bool ContactAddressListModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    auto address = mAddresses->at(index.row());
    auto newValue = value.toString();
    if(role == StreetRole && address->Street() != newValue){
        address->setStreet(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    if(role == CityRole && address->City() != newValue){
        address->setCity(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    if(role == StateRole && address->State() != newValue){
        address->setState(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    if(role == ZipRole && address->ZipCode() != newValue){
        address->setZipCode(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    if(role == TypeRole && address->Type() != newValue){
        address->setType(newValue);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ContactAddressListModel::flags(const QModelIndex &index) const {
    if(!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

bool ContactAddressListModel::insertRows(int row, int count, const QModelIndex &parent) {
    beginInsertRows(parent, row, row + count - 1);
    if(row == mAddresses->count() - 1)
        for(int i = 0; i < count; i++)
            mAddresses->push_back(QSharedPointer<ContactAddress>::create());
    else
        for(int i = 0; i < count; i++)
            mAddresses->insert(row+i, QSharedPointer<ContactAddress>::create());
    endInsertRows();
	return true;
}

bool ContactAddressListModel::removeRows(int row, int count, const QModelIndex &parent) {
    beginRemoveRows(parent, row, row + count - 1);
    mAddresses->remove(row, count);
    endRemoveRows();
	return true;
}

QHash<int, QByteArray> ContactAddressListModel::roleNames() const {
    QHash<int, QByteArray> names;
    names[StreetRole]  = "street";
    names[CityRole]    = "city";
    names[StateRole]   = "state";
    names[ZipRole]     = "zip";
    names[TypeRole]    = "type";
    return names;
}

void ContactAddressListModel::setAddresses(QSharedPointer<QVector<QSharedPointer<ContactAddress>>> addresses) {
    beginResetModel();
    mAddresses = addresses;
    endResetModel();
}

void ContactAddressListModel::invalidateData() {
    mAddresses = nullptr;
}

void ContactAddressListModel::resetModel() {
    beginResetModel();
    endResetModel();
}
