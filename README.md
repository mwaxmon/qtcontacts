# Qt Contacts App Clone

This is my personal experimental project that's meant to allow me to explore the more advanced features of the Qt framework with respect to integration within an application given my personal requirements.

## Features

* Sort and search contacts by name.

![search gif](/docs/Search.gif)

* Store numbers, email addresses, physical addresses.

![contact information](/docs/Number_Email.gif)

* View physical address on a map.

![map](/docs/Map.gif)

## System Requirements
Because this is a Qt C++ application, there should be no other requirements other than those listed on the [Qt website](https://doc.qt.io/qt-5/supported-platforms-and-configurations.html) to deploy the application.

Currently compiles under the MinGW and MSVC Qt toolchains on Windows 10 (No issues with CLion but currently, there is an issue with debugging in Visual Studio not locating the debug built libs, more testing needed for VS integration).

Because of a bug in the Qt framework (either Positioning or Location library), this application requires Qt version 5.11 to produce a usable binary on Windows 10 April 2018 update.  Further testing is needed to determine the lowest possible version of Qt for other platforms to compile.

Because of the various possible locations of Qt toolchains, the root CMakeLists.txt must be modified for your platform/toolchain to locate the proper folder.

## Background

As stated above, this is my personal test project to really dig into the intricate/undocumented requirements of Qt application development and where no documented best practices exist such as:

* Supporting multiple models with the same backing data.
* When to use Qt and when to use standard C++ libs.
* How best to link QML with C++.
* How to integrate a pre-existing python library into C++ (just meant for experimenting; depending on the interface, do not do this unless absolutely necessary).

## Requirements

While developing this project, I try to follow a few strict guidelines:

* The project must be cross platform and development be supported by most common IDEs.
* The UI code must be as minimalist as possible and written using QML.
* There must be a built-in test harness for both UI and C++ code.
* The UI will not touch the data directly.
* The project can update to the latest compiler/framework quickly.

## Roadmap

Along with adding features common to contact management applications, there are some things that I had planned to support and as the application grew, some things had to be dropped and others need to be added:

* Integration with the Python library that synchronized data with a remote server (again, do not do this!).
* Better maps support (Using Javascript/HTML5 - https://developers.google.com/maps/documentation/javascript/tutorial).
* Consistent model setup (looking at using a tree based model to reduce need for multiple similar models).
* Better handling of UI signals (Currently the C++ code references three different views by name to get their signals.  Should it be one?  And should the main UI window be the sole interface?).
* Better SQLite integration (potentially using indexes - https://www.sqlite.org/queryplanner.html)